package me.darkhost.saveyourdata.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;

/**
 * Created by 민재 on 2016-12-17.
 */

public class Util {
    public static boolean isSystemPackage(ApplicationInfo applicationInfo) {
        // 시스템 앱일경우
        if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
            // 만약 업데이트가 된 적 있는 시스템 앱이라면 false
            if ((applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0)
                return false;
                // 아니라면 true
            else return true;
        }
        // 시스템 앱이 아닐경우 false
        else return false;
    }

    // 서비스 실행 여부 체크
    // http://stroot.tistory.com/43
    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // UsageStats 권한 소유 여부 체크
    // http://stackoverflow.com/questions/28921136/how-to-check-if-android-permission-package-usage-stats-permission-is-given
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean hasUsageStatsPermission(Context context) {
        AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), context.getPackageName());
        boolean granted = mode == AppOpsManager.MODE_ALLOWED;
        return granted;
    }

    // Dp To Pixel
    // http://stackoverflow.com/questions/23389305/android-createscaledbitmap-issue
    public static float convertDpToPixel(float dp, Activity context)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    // Package Name으로 App Label을 가져오는 작업
    public static String getApplicationLabelFromPackageName(Context context, String AppPackage) {
        String AppLabel = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            AppLabel = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(AppPackage, PackageManager.GET_META_DATA));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return AppLabel;
    }

    // Package Name으로 App Icon을 가져오는 작업
    public static Drawable getApplicationIconFromPackageName(Context context, String AppPackage) {
        Drawable AppIcon = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            AppIcon = packageManager.getApplicationIcon(packageManager.getApplicationInfo(AppPackage, PackageManager.GET_META_DATA));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return AppIcon;
    }

    // 알림 삭제
    public static void cancelNotification(Context context, int notifyNumber) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notifyNumber);
    }

    // get Array's Index
    public static int getArrayIndex(Context context, int array, String findIndex) {
        String[] arrayString = context.getResources().getStringArray(array);
        for (int e = 0; e < arrayString.length; e++) {
            if (arrayString[e].equals(findIndex))
                return e;
        }
        return -1;
    }

    // Android O (API 26) 이상에서 앱 아이콘 가져오기
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static Bitmap getApplicationIconFromPackageName_O(PackageManager packageManager, String packageName) {
        try {
            Drawable drawable = packageManager.getApplicationIcon(packageName);

            if (drawable instanceof BitmapDrawable) {
                return ((BitmapDrawable) drawable).getBitmap();
            } else if (drawable instanceof AdaptiveIconDrawable) {
                Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
                Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

                Drawable[] drr = new Drawable[2];
                drr[0] = backgroundDr;
                drr[1] = foregroundDr;

                LayerDrawable layerDrawable = new LayerDrawable(drr);

                int width = layerDrawable.getIntrinsicWidth();
                int height = layerDrawable.getIntrinsicHeight();

                Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

                Canvas canvas = new Canvas(bitmap);

                layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                layerDrawable.draw(canvas);

                return bitmap;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
