package me.darkhost.saveyourdata.customview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import me.darkhost.saveyourdata.R;
import me.darkhost.saveyourdata.db.DBManager;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by 민재 on 2016-12-29.
 */

public class appListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<appListItem> mListData = new ArrayList<>();
    private DBManager dbManager;
    private HashMap<Integer,Boolean> checkMap = new HashMap<>();

    public appListAdapter(Context context) {
        super();
        this.context = context;

        // SQLite DB 로드
        dbManager = new DBManager(context, "applist.db", null, 1);
    }

    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int i) {
        return mListData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final appListItem appListItem = mListData.get(pos);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.select_listview, parent, false);
                holder = new ViewHolder();
                holder.PackageName = (TextView) convertView.findViewById(R.id.packagenames);
                holder.Label = (TextView) convertView.findViewById(R.id.applabel);
                holder.Icon = (ImageView) convertView.findViewById(R.id.appicon);
                holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.PackageName.setText(appListItem.PackageName);
            holder.PackageName.setTag(pos);
            holder.Label.setText(appListItem.Label);
            holder.Label.setTag(pos);

            if (appListItem.Icon != null) {
                holder.Icon.setVisibility(View.VISIBLE);
                holder.Icon.setImageDrawable(appListItem.Icon);
            } else {
                holder.Icon.setVisibility(View.GONE);
            }

            holder.checkBox.setTag(appListItem.PackageName);

            checkMap.put(pos, false);
            ArrayList<String> applist = dbManager.getAllTargetData();
            for(String app : applist) {
                if(app.equals(holder.checkBox.getTag())) {
                    checkMap.put(pos, true);
                    Log.d("appListAdapter", holder.checkBox.getTag() + " True");
                }
            }

            if(checkMap.get(pos)) {
                Log.d("appListAdapter", holder.checkBox.getTag() + " Checked");
                holder.checkBox.setChecked(true);
            } else {
                Log.d("appListAdapter", holder.checkBox.getTag() + " UnChecked");
                holder.checkBox.setChecked(false);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public void addItem(String Package, String Label, Drawable Icon) {
        appListItem addInfo;
        addInfo = new appListItem();
        addInfo.PackageName = Package;
        addInfo.Label = Label;
        addInfo.Icon = Icon;

        mListData.add(addInfo);
    }

    public void sort() {
        Collections.sort(mListData, appListItem.ALPHA_COMPARATOR);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView PackageName;
        TextView Label;
        ImageView Icon;
        CheckBox checkBox;
    }
}

class appListItem {
    public String PackageName;
    public String Label;
    public Drawable Icon;

    public static final Comparator<appListItem> ALPHA_COMPARATOR = new Comparator<appListItem>() {
        private final Collator collator = Collator.getInstance();

        @Override
        public int compare(appListItem appListItem, appListItem t1) {
            return collator.compare(appListItem.Label, t1.Label);
        }
    };
}
