package me.darkhost.saveyourdata.service;

import android.app.Notification;
import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import me.darkhost.saveyourdata.receiver.ScreenReceiver;
import me.darkhost.saveyourdata.receiver.UpdateReceiver;
import me.darkhost.saveyourdata.util.Util;

/**
 * Created by 민재 on 2017-01-06.
 */

public class ServiceControllerService extends Service {

    // 스크린 리시버
    IntentFilter screenReceiverfilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
    ScreenReceiver screenReceiver = new ScreenReceiver();

    // 네트워크 감지 리시버
    public static int NetworkJob_CODE = 1000;
    JobScheduler Networkjs;

    // 업데이트 리시버
    UpdateReceiver updateReceiver = new UpdateReceiver();
    IntentFilter updateFilter = new IntentFilter(Intent.ACTION_PACKAGE_REPLACED);

    // SP
    SharedPreferences sharedPreferences;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        Log.d("ServiceController", "ServiceControllerService Created!");

        // 활성화 정보 가져오기
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean isEnabled = sharedPreferences.getBoolean("enabledSwc", true);

        // 연결된 네트워크 방법 체크
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        // 연결된 네트워크가 모바일 네트워크일경우
        if(networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
            // 서비스가 멈춰있는 상태이고 활성화일경우
            if (!Util.isMyServiceRunning(this, WarningService.class) && isEnabled) {
                // 서비스 시작
                Log.d("ServiceController", "WarningService Created!");
                Intent i = new Intent("me.darkhost.saveyourdata.warningservice");
                i.setPackage("me.darkhost.saveyourdata");
                Log.d("ServiceController", "Service Start");
                if (Build.VERSION.SDK_INT >= 26) {
                    startForegroundService(i);
                }
                else startService(i);
            }
        }

        // 스크린 리시버 등록
        screenReceiverfilter.addAction(Intent.ACTION_SCREEN_OFF);
        this.registerReceiver(screenReceiver, screenReceiverfilter);
        sendBroadcast(new Intent("me.darkhost.saveyourdata.screenreceiver"), "android.intent.action.SCREEN_ON");
        sendBroadcast(new Intent("me.darkhost.saveyourdata.screenreceiver"), "android.intent.action.SCREEN_OFF");

        // 네트워크 감지 Job 등록
        JobInfo NetworkJob = new JobInfo.Builder(NetworkJob_CODE,
                new ComponentName(getApplicationContext(), NetworkJobService.class))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .build();
        Networkjs = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        Networkjs.schedule(NetworkJob);

        // 업데이트 리시버 등록
        updateFilter.addDataScheme("package");
        this.registerReceiver(updateReceiver, updateFilter);
    }

    public void onDestroy() {
        Log.d("ServiceController", "ServiceControllerService Destoryed!");

        // 스크린 리시버 해제
        this.unregisterReceiver(screenReceiver);

        // 네트워크 감지 Job 해제
        Networkjs.cancel(NetworkJob_CODE);

        // 업데이트 리시버 해제
        this.unregisterReceiver(updateReceiver);

        // 혹시 모르니 노티 알림도 삭제
        Util.cancelNotification(getApplicationContext(), Notification.PRIORITY_HIGH);
        Util.cancelNotification(getApplicationContext(), 2);

        // 서비스가 작동중일경우
        if(Util.isMyServiceRunning(this, WarningService.class)) {
            // 서비스 종료
            Log.d("ServiceController", "WarningService Destoryed!");
            Intent i = new Intent("me.darkhost.saveyourdata.warningservice");
            i.setPackage("me.darkhost.saveyourdata");
            stopService(i);
        }
    }
}
