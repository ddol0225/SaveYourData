package me.darkhost.saveyourdata.service

import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.Intent
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import me.darkhost.saveyourdata.util.Util

/*
    비과금 네트워크에서 작동하는 JobService
    2018-08-14 by Darkhost (Minjae Seon)
 */
class NetworkJobService : JobService() {
    override fun onStartJob(params: JobParameters?): Boolean {
        val pref = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val isEnabled = pref.getBoolean("enabledSwc", true)
        // 서비스가 작동중이라면 (이때는 활성화 체크 불필요 -> 어차피 비활성화여도 켜져있으면 꺼줘야함)
        if (Util.isMyServiceRunning(application, WarningService::class.java)) {
            // 서비스 종료
            Log.d("NetworkJobService", "WarningService Destoryed!")
            stopService(Intent(applicationContext, WarningService::class.java))
        }
        return isEnabled
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        val pref = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val isEnabled = pref.getBoolean("enabledSwc", true)
        // 서비스가 작동중이지 않고 활성화 상태라면
        if (!Util.isMyServiceRunning(application, WarningService::class.java) && isEnabled) {
            // 서비스 실행
            Log.d("NetworkJobService", "WarningService Created!")
            if (Build.VERSION.SDK_INT >= 26) {
                startForegroundService(Intent(applicationContext, WarningService::class.java))
            } else {
                startService(Intent(applicationContext, WarningService::class.java))
            }
        }
        return isEnabled
    }
}