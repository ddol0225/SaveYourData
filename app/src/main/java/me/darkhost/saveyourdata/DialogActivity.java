package me.darkhost.saveyourdata;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import me.darkhost.saveyourdata.util.Util;

/**
 * Created by 민재 on 2016-12-11.
 */

public class DialogActivity extends Activity {
    boolean isNo = false;
    boolean isEnabled = true;
    @Override
    protected void onCreate(Bundle saveInstanceState) {
        // No 버튼을 눌렀는지 안눌렀는지 가져옴 (기본값은 false)
        SharedPreferences selectNo = getSharedPreferences("SelectNo", MODE_PRIVATE);
        isNo = selectNo.getBoolean("SelectNo", false);

        // 활성화 되어있는지 확인하기 위해 가져옴 (기본값은 true)
        SharedPreferences enabledSwc = getSharedPreferences("enabledSwc", MODE_PRIVATE);
        isEnabled = enabledSwc.getBoolean("enabledSwc", true);

        // Service Infomation
        final Intent i = new Intent("me.darkhost.saveyourdata.servicecontrollerservice");
        i.setPackage("me.darkhost.saveyourdata");

        // 활성화 되어있을 경우
        if (isEnabled) {
            // No 버튼을 이전에 누르지 않았을경우 (false)
            if (!isNo) {
                // Dialog View를 생성함
                super.onCreate(saveInstanceState);
                requestWindowFeature(Window.FEATURE_NO_TITLE);
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                layoutParams.dimAmount = 0.7f;
                getWindow().setAttributes(layoutParams);
                setContentView(R.layout.dialog);

                // Dialog의 텍스트를 변경하기 위함
                TextView WarningText = (TextView) findViewById(R.id.dialogtext);

                // Dialog를 요청한 곳에서 넘겨준 정보를 받기 위해 작성
                Intent intent = getIntent();

                // DialogType - 다이얼로그가 어디에 쓰일지 체크하기 위해 필수적으로 받아와야 함
                // 0 : 경고 메세지
                int DialogType = intent.getIntExtra("DialogType", 0);

                if(DialogType == 0) {

                    // 경고 서비스에서 패키지명을 받아옴
                    String AppPackage = intent.getStringExtra("AppPackage");

                    // 앱 이름을 패키지 이름으로 가져옴
                    String AppLabel = Util.getApplicationLabelFromPackageName(getApplicationContext(), AppPackage);

                    // 정상적으로 앱 이름을 가져온경우
                    if (AppLabel != null) {
                        WarningText.setText("\'" + AppLabel + "\' " + getString(R.string.networkwarning_appname));
                    }
                    // 앱 이름을 가져오는데 실패했을 경우
                    else {
                        WarningText.setText(getString(R.string.networkwarning_noappname));
                    }

                    // Yes 버튼 ClickListener
                    Button yes = (Button) findViewById(R.id.yesbutton);
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                // 앱에서 직접적으로 끄는 방법이 (아마도) 없으므로 데이터 설정창으로 이동함
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                                } else {
                                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$WirelessSettings"));
                                }
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), getString(R.string.showsettingerror), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                            finish();
                        }
                    });

                    // No 버튼 ClickListener
                    Button no = (Button) findViewById(R.id.nobutton);
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // No를 누르면 No를 눌렀던 정보가 저장됨
                            // 초기화 과정은 네트워크 연결 방식이 바뀌었을 경우 (Data -> Wi-Fi 등)
                            SharedPreferences selectNo = getSharedPreferences("SelectNo", MODE_PRIVATE);
                            SharedPreferences.Editor editor = selectNo.edit();
                            editor.putBoolean("SelectNo", isNo);
                            editor.apply();

                            // No를 눌렀으므로 서비스를 끔
                            stopService(i);

                            // 액티비티를 마침
                            finish();
                        }
                    });
                }
            }
            else
            {
                // 아니오 버튼을 이전에 눌렀을경우 서비스를 끔
                stopService(i);
            }
        }
        else
        {
            // 활성화가 되있지 않은데 서비스가 작동한 경우 서비스를 꺼버림
            stopService(i);
        }
    }
}
