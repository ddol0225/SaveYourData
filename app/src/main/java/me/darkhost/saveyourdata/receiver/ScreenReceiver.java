package me.darkhost.saveyourdata.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import me.darkhost.saveyourdata.service.WarningService;
import me.darkhost.saveyourdata.util.Util;

/**
 * Created by 민재 on 2017-01-06.
 */

public class ScreenReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // 스크린이 켜지는 액션일경우
        if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.d("ScreenReceiver", "Screen On Receive!");
            // 서비스 상태 체크
            if (!Util.isMyServiceRunning(context, WarningService.class)) {
                // 서비스 시작
                   Log.d("ScreenReceiver", "WarningService Start!");
                Intent i = new Intent("me.darkhost.saveyourdata.warningservice");
                i.setPackage("me.darkhost.saveyourdata");
                if (Build.VERSION.SDK_INT >= 26) {
                    context.startForegroundService(i);
                }
                else context.startService(i);
            }
        }
        // 스크린이 꺼지는 액션일경우
        else if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.d("ScreenReceiver", "Screen Off Receive!");
            // 서비스 상태 체크
            if(Util.isMyServiceRunning(context, WarningService.class)) {
                // 서비스 종료
                Log.d("ScreenReceiver", "WarningService Stop!");
                context.stopService(new Intent(context, WarningService.class));
            }
        }
    }
}
