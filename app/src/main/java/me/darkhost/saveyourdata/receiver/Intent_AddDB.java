package me.darkhost.saveyourdata.receiver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import me.darkhost.saveyourdata.R;
import me.darkhost.saveyourdata.db.DBManager;
import me.darkhost.saveyourdata.util.Util;

/**
 * Created by 민재 on 2017-06-04.
 */

public class Intent_AddDB extends Activity {
    DBManager dbManager;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.empty);

        // 어떤 앱을 추가해야할지 체크하기 위하여 Intent 정보 받아옴
        intent = getIntent();

        // 받아온 정보에서 AppPackage 이름을 받아옴
        String AppPackage = intent.getStringExtra("AppPackage");
        boolean isAddDB = intent.getBooleanExtra("AddBool", true);

        try {
            // DB에 정보 추가
            if(isAddDB) {
                AddDB(AppPackage, true);
                Toast.makeText(getApplicationContext(), getString(R.string.DBAddIntent_Complete), Toast.LENGTH_SHORT).show();
            }
            else {
                AddDB(AppPackage, false);
            }
        }
        // 추가 실패시
        catch (Exception e) {
            // 오류 메세지 출력
            e.printStackTrace();
            if(isAddDB) {
                Toast.makeText(getApplicationContext(), getString(R.string.DBAddIntent_Failed), Toast.LENGTH_SHORT).show();
            }
        }
        Util.cancelNotification(getApplicationContext(), 2);
        finish();
    }

    public void AddDB(String AppPackage, boolean bool) {
        // SQLite DB 로드
        dbManager = new DBManager(Intent_AddDB.this, "applist.db", null, 1);
        String value = String.valueOf(bool);
        Log.d("Intent_AddDB", AppPackage + " REG : " + value);
        if(bool) {
            if(!dbManager.getExistPackage(AppPackage)) {
                dbManager.AppDataInsert(AppPackage, true);
            }
        }
        else {
            dbManager.AppDataDelete(AppPackage);
        }
    }
}
